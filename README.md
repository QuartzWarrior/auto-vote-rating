# Auto Vote Rating - Chrome Extension
<details>
<summary>List of sites that the extension supports</summary>
<a href="http://topcraft.ru/">TopCraft.ru</a>  
<a href="https://mctop.su/">McTOP.su</a>  
<a href="http://mcrate.su/">MCRate.su</a>  
<a href="http://minecraftrating.ru/">MinecraftRating.ru</a>  
<a href="http://monitoringminecraft.ru/">MonitoringMinecraft.ru</a>  
<a href="https://ionmc.top/">IonMc.top</a>  
<a href="https://minecraftservers.org/">MinecraftServers.org</a>  
<a href="https://serveur-prive.net/minecraft">Serveur-Prive.net</a>  
<a href="https://www.planetminecraft.com/">PlanetMinecraft.com</a>  
<a href="https://topg.org/Minecraft">TopG.org</a>  
<a href="https://minecraft-mp.com/">Minecraft-Mp.com</a>  
<a href="http://minecraft-server-list.com/">Minecraft-Server-List.com</a>  
<a href="https://www.serverpact.com/">ServerPact.com</a>  
<a href="https://www.minecraftiplist.com/">MinecraftIpList.com</a>  
<a href="https://topminecraftservers.org/">TopMinecraftServers.org</a>  
<a href="http://minecraftservers.biz/">MinecraftServers.biz</a>  
<a href="https://hotmc.ru/">HotMC.ru</a>  
<a href="https://minecraft-server.net/">Minecraft-Server.net</a>  
<a href="https://top-games.net/">Top-Games.net или Top-Serveurs.net</a>  
<a href="https://tmonitoring.com/">TMonitoring.com</a>  
<a href="https://top.gg/">Top.GG</a>  
<a href="https://discordbotlist.com/">DiscordBotList.com</a>  
<a href="https://discords.com/">Discords.com</a>  
<a href="https://mmotop.ru/">MMoTop.RU</a>  
<a href="https://mc-servers.com/">MC-Servers.com</a>  
<a href="https://minecraftlist.org/">MinecraftList.org</a>  
<a href="https://www.minecraft-index.com/">Minecraft-Index.com</a>  
<a href="https://serverlist101.com/">ServerList101.com</a>  
<a href="https://mcserver-list.eu/">MCServer-List.eu</a>  
<a href="https://craftlist.org/">CraftList.org</a>  
<a href="https://czech-craft.eu/">Czech-Craft.eu</a>  
<a href="https://minecraft.buzz/">Minecraft.buzz</a>  
<a href="https://minecraftservery.eu/">MinecraftServery.eu</a>  
<a href="https://www.rpg-paradize.com/">RPG-Paradize.com</a>  
<a href="https://www.minecraft-serverlist.net/">Minecraft-ServerList.net</a>  
<a href="https://minecraft-server.eu/">Minecraft-Server.eu</a>  
<a href="https://www.minecraftkrant.nl/">MinecraftKrant.nl</a>  
<a href="https://www.trackyserver.com/">TrackyServer.com</a>  
<a href="https://mc-lists.org/">MC-Lists.org</a>  
<a href="https://topmcservers.com/">TopMCServers.com</a>  
<a href="https://bestservers.com/">BestServers.com</a>  
<a href="https://craft-list.net/">Craft-List.net</a>  
<a href="https://www.minecraft-servers-list.org/">Minecraft-Servers-List.org</a>  
<a href="https://www.serverliste.net/">ServerListe.net</a>  
<a href="https://gtop100.com/">GTop100.com</a>  
<a href="https://wargm.ru/s">WARGM.ru</a>  
<a href="https://minestatus.net/">MineStatus.net</a>  
<a href="https://misterlauncher.org/">MisterLauncher.org</a>  
<a href="https://minecraft-servers.de/">Minecraft-Servers.de</a>  
<a href="https://discord.boats/">Discord.Boats</a>  
<a href="https://serverlist.games/">ServerList.Games</a>  
<a href="https://best-minecraft-servers.co/">Best-Minecraft-Servers.co</a>  
<a href="https://minecraftservers100.com/">MinecraftServers100.com</a>  
<a href="https://mc-serverlist.cz/">MC-ServerList.cz</a>  
<a href="https://mineservers.com/">MineServers.com</a>  
<a href="https://atlauncher.com/">ATLauncher.com</a>  
<a href="https://servers-minecraft.net/">Servers-Minecraft.net</a>  
</details>

### Links to the extension where you can install it:
[Chrome Web Store](https://chrome.google.com/webstore/detail/auto-vote-minecraft-ratin/mdfmiljoheedihbcfiifopgmlcincadd)   
[Firefox Add-ons](https://addons.mozilla.org/ru/firefox/addon/auto-vote-rating/)   
[Opera Addons](https://addons.opera.com/ru/extensions/details/auto-vote-minecraft-rating/)   
[Microsoft Edge Add-ons](https://microsoftedge.microsoft.com/addons/detail/auto-vote-rating/ecoifpgiojfhmihcfomafdcmkphafpba)
## Install the extension from zip archive
Here is a short guide how to install an unpacked extension on Google Chrome or Chromium-based browsers
1. Unzip the archive to any convenient location
2. Open the "Extensions" page (`chrome://extensions/`) in the browser and turn on the "Developer mode"

![](https://i.imgur.com/iQ4DXVu.png)

3. Click on the "Load unpacked" button and select the directory where you unpacked the archive.